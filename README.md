## Before Install
You must have installed NodeJS and git on your local computer or laptop

  1. Download and Install NodeJS [here](https://nodejs.org/en/download/ "here")
  2. Download and Install git [here](https://git-scm.com/downloads "here")
  3. And also don't forget to install Visual Studio Code for update of source code [here](https://code.visualstudio.com/Download "here")

## How to Install
1. Clone repository
    ```
    git clone https://oimtrust@bitbucket.org/oimtrust/gokomodotest.git
    ```
2. Install Dependencies
    ```
    npm install
    ```
3. Run cypress using GUI
    ```
    npm run cy:open
    ```
4. Run cypress using Headless Browser
    ```
    npm run cy:run
    ```