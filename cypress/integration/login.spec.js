/// <reference types="cypress" />

context('Automate Test for Login', () => {
    const EMAIL         = '#input-email'
    const PASSWORD      = '#input-password'
    const LOGIN         = 'button[type="submit"]'
    const ERROR_MESSAGE = '.text-danger'

    beforeEach(() => {
        cy.visit('http://localhost:3000/')
    });

    it('If login using the wrong email/password it will be rejected', () => {
        cy.get(EMAIL).type('komodo@komodo.com').should('have.value', 'komodo@komodo.com')
        cy.get(PASSWORD).type('wrong_password').should('have.value', 'wrong_password')
        cy.get(LOGIN).click()
        cy.get(ERROR_MESSAGE).should('contain.text', 'Unprocessable Entity')
    });
    
    it('If login using non-active account it will be rejected', () => {
        cy.get(EMAIL).type('testing@example.com').should('have.value', 'testing@example.com')
        cy.get(PASSWORD).type('testing123').should('have.value', 'testing123')
        cy.get(LOGIN).click()
        cy.get(ERROR_MESSAGE).should('contain.text', 'Unauthorized')
    });
    
    it('If login using true email/password it will be redirect to dashboard', () => {
        cy.get(EMAIL).type('komodo@example.com').should('have.value', 'komodo@example.com')
        cy.get(PASSWORD).type('komodo123').should('have.value', 'komodo123')
        cy.get(LOGIN).click()
        cy.get('h2').should('contain.text', 'Hello')
    });
});